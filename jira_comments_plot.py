from jira import JIRA
import creds as CREDS
import sys
import re
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
from collections import Counter
# from collections import OrderedDict


def main():
    # Make sure we have a name to lookup
    if len(sys.argv) < 2:
        print "Give me a name"
        sys.exit()

    commenter = sys.argv[1]

    # init plotly
    plotly.tools.set_credentials_file(
            username=CREDS.plot_api_username,
            api_key=CREDS.plot_api_key)

    # connect to jira
    servername = CREDS.server
    options = {'server': servername}
    user = CREDS.user
    password = CREDS.password
    jira = JIRA(options, basic_auth=(user, password))

    # this will be the list after we get what we want
    list_of_all = []
    count_of_all = 0

    # get our issues
    list_of_issues = jira.search_issues(
                    'created > startofmonth()',
                    startAt=0,
                    maxResults=50000,
                    fields='reporter,summary,comment,created,updated')
    print "list_of_issues.total = " + str(list_of_issues.total)

    # if our results are too long, we have to get them in batches
    if list_of_issues.total > 1000:
        print "That's more than 1000 results! Lets Break them up!"
        count_total_issues = list_of_issues.total
        startAtIterator = 0
        list_of_issues = []
        while startAtIterator < count_total_issues:
            list_of_issues = jira.search_issues(
                            'created > startofmonth()',
                            startAt=startAtIterator,
                            maxResults=1000,
                            fields='reporter,summary,comment,created,updated')
            print "Working on : " + str(startAtIterator) + \
                "/" + str(count_total_issues) + " results."
            startAtIterator += 1000

            # if our commenter is either the reporter or the commenter
            list_of_all, count_of_all = match_my_commenter(list_of_issues, commenter, list_of_all, count_of_all)
    else:
        list_of_all, count_of_all = match_my_commenter(list_of_issues, commenter, list_of_all, count_of_all)

    # take what we got and organize it into a dictionary and sort
    count_dict = Counter(list_of_all)
    just_dict = {}
    for key, value in count_dict.iteritems():
        just_dict[key] = value

    # sorted_dict = OrderedDict(sorted(just_dict.items(), key=lambda t: t[0]))
    # plotted = plot_my_dict(sorted_dict)
    # for key,value in sorted_dict.items():
    #    print key, value

    print "Way to go " + str(commenter) + \
        ".  You've worked on " + str(count_of_all) + " tickets."


def match_my_commenter(list_of_issues, commenter, list_of_all, count_of_all):
    for issue in list_of_issues:
        if issue.fields.reporter.name == commenter:
            list_of_all.append(issue.fields.created[:10])
            count_of_all += 1
        for comment in issue.fields.comment.comments:
            if re.search(commenter, comment.author.name):
                list_of_all.append(comment.updated[:10])
                count_of_all += 1
    return list_of_all, count_of_all


def plot_my_dict(mydict):
    xx = []
    yy = []

    for key, value in mydict.iteritems():
        xx.append(key)
        yy.append(value)

    data = [go.Bar(x=xx, y=yy)]
    py.plot(data, filename='work-bar')
    return

main()
